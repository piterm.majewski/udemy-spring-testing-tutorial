package pl.pmajewski.udemy.min;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.Test;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;

class JsonPathTest {

	@Test
	void test() {
		String response = "[{\"id\" : 1, \"name\": \"Caprica-Six\", \"sex\" : \"female\"},{\"id\" : 2, \"name\": \"Gaius Baltar\", \"sex\" : \"male\"},{\"id\" : 3, \"name\": \"William Adama\", \"sex\" : \"male\"}]";
		DocumentContext context = JsonPath.parse(response);
		
		int length = context.read("$.length()");
		assertThat(length).isEqualTo(3);
		
		List<Integer> ids = context.read("$..id");
		System.out.println("ids: "+ids);
		assertThat(ids).containsExactly(1, 2, 3);
		
		System.out.println("1: "+context.read("$.[1]").toString());
		System.out.println("2: "+context.read("$.[0:1]").toString());
		System.out.println("3: "+context.read("$.[?(@.name=='Caprica-Six')]").toString());
		System.out.println("4: "+context.read("$.[?(@.sex=='male')]").toString());
	}
}
