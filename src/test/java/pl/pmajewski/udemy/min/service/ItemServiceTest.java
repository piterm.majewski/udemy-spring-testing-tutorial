package pl.pmajewski.udemy.min.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.Arrays;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import pl.pmajewski.udemy.min.model.Item;
import pl.pmajewski.udemy.min.repository.ItemRepository;

@ExtendWith(MockitoExtension.class)
class ItemServiceTest {

	@InjectMocks
	private ItemService itemService;
	
	@Mock
	private ItemRepository itemRepo;
	
	@Test
	void findAllTest() {
		when(itemRepo.findAll()).thenReturn(Arrays.asList(new Item(1, "Item1", 1,1 ), new Item(2, "Item2", 1, 2)));
		
		assertEquals(1, itemService.findAll().get(0).getValue());
		assertEquals(2, itemService.findAll().get(1).getValue());
	}
}
