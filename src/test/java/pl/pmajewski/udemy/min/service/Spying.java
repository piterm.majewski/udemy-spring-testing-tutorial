package pl.pmajewski.udemy.min.service;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;

@ExtendWith(MockitoExtension.class)
public class Spying {

	/*
	 * Mock - doesn't retain original behavior
	 */
	@Test
	public void mocking() {
		System.out.println("=== MOCK ===");
		ArrayList mock = Mockito.mock(ArrayList.class);
		System.out.println(mock.size()); // 0
		Object object = mock.get(0); // null
		mock.add("Hello Mock"); // do nothing
		System.out.println(mock.size()); // 0
	}
	
	/*
	 * Spy - retain original behavior
	 */
	@Test
	public void spying() {
		System.out.println("=== SPY ===");
		ArrayList spy = Mockito.spy(ArrayList.class);
		System.out.println(spy.size()); // 0
//		Object object = spy.get(0); // exception - element doesn't exists
		spy.add("Hello Spy"); // do nothing
		System.out.println(spy.size()); // 1
	}
}
