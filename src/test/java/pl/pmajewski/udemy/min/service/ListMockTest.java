package pl.pmajewski.udemy.min.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.jupiter.api.Test;


public class ListMockTest {

	List mock = mock(List.class);
	
	@Test
	public void simpleSizeTest() {
		when(mock.size()).thenReturn(5);
		assertEquals(5, 5);
	}

	@Test
	public void advancedSizeTest() {
		when(mock.size()).thenReturn(5).thenReturn(10);
		assertEquals(5, mock.size());
		assertEquals(10, mock.size());
	}	
	
	@Test
	public void stringMock() {
		when(mock.get(0)).thenReturn("BattlestarGalactica");
		assertEquals("BattlestarGalactica", mock.get(0));
	}
	
	@Test
	public void anyString() {
		when(mock.get(anyInt())).thenReturn("StarGate");
		assertEquals("StarGate", mock.get(0));
		assertEquals("StarGate", mock.get(1));
		assertEquals("StarGate", mock.get(10));
		assertEquals("StarGate", mock.get(100));
		
	}
	
}
