package pl.pmajewski.udemy.min.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;

import pl.pmajewski.udemy.min.repository.NumberRepository;

@ExtendWith(MockitoExtension.class)
class SomeServiceMockTest {

	@Mock
	private NumberRepository numberRepoMock;
	@InjectMocks
	private SomeService someService = new SomeService();
	
	@Test
	@DisplayName("Full of data")
	void testCalcSumAllDatabase() {
		when(numberRepoMock.getAll()).thenReturn(new int[] {1, 2, 3});
		assertEquals(6, someService.calcSumAllDatabase());
	}

	@Test
	@DisplayName("Empty database")
	void emptyDatabase() {
		when(numberRepoMock.getAll()).thenReturn(new int[] { });
		assertEquals(0, someService.calcSumAllDatabase());
	}
}
