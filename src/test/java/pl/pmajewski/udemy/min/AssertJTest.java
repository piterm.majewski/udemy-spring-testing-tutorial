package pl.pmajewski.udemy.min;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;

class AssertJTest {

	@Test
	void test() {
		List<Integer> numbers = Arrays.asList(2, 3, 4);
		
		assertThat(numbers)
			.hasSize(3)
			.contains(2, 3)
			.allMatch(i -> i > 1);
	}
}
