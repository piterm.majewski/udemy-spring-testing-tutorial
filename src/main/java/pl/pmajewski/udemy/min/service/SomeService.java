package pl.pmajewski.udemy.min.service;

import java.util.Arrays;

import pl.pmajewski.udemy.min.repository.NumberRepository;

public class SomeService {

	private NumberRepository numberRepo;
	
	public SomeService() { }
	
//	public SomeService(NumberRepository numberRepo) {
//		this.numberRepo = numberRepo;
//	}
	
	public int calcSumAll(int[] data) {
		return Arrays.stream(data).sum();
	}

	public int calcSumAllDatabase() {
		return Arrays.stream(numberRepo.getAll()).sum();
	}

	public NumberRepository getNumberRepo() {
		return numberRepo;
	}

	public void setNumberRepo(NumberRepository numberRepo) {
		this.numberRepo = numberRepo;
	}
}
