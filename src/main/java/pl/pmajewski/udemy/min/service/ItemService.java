package pl.pmajewski.udemy.min.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pl.pmajewski.udemy.min.model.Item;
import pl.pmajewski.udemy.min.repository.ItemRepository;

@Service
public class ItemService {

	@Autowired
	private ItemRepository itemRepo;
	
	public Item getItemHardcoded() {
		return new Item(1, "Ball", 10, 10);
	}
	
	public List<Item> findAll() {
		List<Item> all = itemRepo.findAll();
		all.stream().forEach(i -> i.setValue(i.getPrice() * i.getQuantity()));
		
		return all;
	}
}
