package pl.pmajewski.udemy.min.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@ToString
@EqualsAndHashCode(of = "id")
@NoArgsConstructor
public class Item {

	@Id
	private Integer id;
	private String name;
	private Integer price;
	private Integer quantity;

	@Transient
	@Setter
	private int value;
	
	public Item(Integer id) {
		this.id = id;
	}
	
	public Item(Integer id, String name, Integer price, Integer  quantity) {
		this.id = id;
		this.name = name;
		this.price = price;
		this.quantity = quantity;
	}
}
