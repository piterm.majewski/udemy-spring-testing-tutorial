package pl.pmajewski.udemy.min.repository;

public class NumberRepositoryImpl implements NumberRepository {

	@Override
	public int[] getAll() {
		return new int[] {1, 2, 3, 4};
	}
	
}
