package pl.pmajewski.udemy.min.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import pl.pmajewski.udemy.min.model.Item;

public interface ItemRepository extends JpaRepository<Item, Integer> {

}
