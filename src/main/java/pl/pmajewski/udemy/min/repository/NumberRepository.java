package pl.pmajewski.udemy.min.repository;

public interface NumberRepository {

	int[] getAll();

}